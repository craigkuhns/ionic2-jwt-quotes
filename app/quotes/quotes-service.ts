import {Injectable} from 'angular2/core';
import {AuthHttp} from 'angular2-jwt';
import 'rxjs/add/operator/map';

@Injectable()
export class QuotesService {
  publicQuoteUrl: string = "http://localhost:3001/api/protected/random-quote";
  
  constructor(private http: AuthHttp) {}
  
  getQuote(): any {
    let response = this.http.get(this.publicQuoteUrl).map(res => res.text()); 
    return response;
  }
}