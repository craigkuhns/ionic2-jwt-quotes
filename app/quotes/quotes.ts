import {Page, NavController} from 'ionic-framework/ionic';
import {QuotesService} from './quotes-service';
import {AuthenticatedPageInterface} from '../authentication/authenticated-page-interface';
import {AuthenticationService} from '../authentication/authentication-service';
import {RequiresAuthentication} from '../authentication/requires-authentication';

@Page({
  templateUrl: 'build/quotes/quotes.html',
  providers: [QuotesService]
})
@RequiresAuthentication()
export class QuotesPage implements AuthenticatedPageInterface {
  quote: string;
  error: string;
  
  constructor(
    private quotesService: QuotesService,
    private authenticationService: AuthenticationService,
    private nav: NavController
  ) {}
  
  resetQuote() {
    this.quote = '';
    this.error = '';
  }
  
  onGetQuote() {
    this.resetQuote();
    
    this.quotesService.getQuote().subscribe(
      data => this.quote = data,
      err => this.error = JSON.stringify(err)
    );
  }
}