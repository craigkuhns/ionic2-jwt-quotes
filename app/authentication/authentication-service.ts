import {Injectable} from 'angular2/core';
import {Storage, LocalStorage} from 'ionic-framework/ionic'
import {Http, Headers} from 'angular2/http';
import {JwtHelper, tokenNotExpired} from 'angular2-jwt';
import {LoginPage} from './login/login';
import 'rxjs/Rx';;
import {Observable} from 'rxjs/Observable';


@Injectable()
export class AuthenticationService {
  loginUrl: string = "http://localhost:3001/sessions/create";
  signupUrl: string = "http://localhost:3001/users"
  contentHeader: Headers = new Headers({"Content-Type": "application/json"});
  jwtHelper: JwtHelper = new JwtHelper();
  local: Storage = new Storage(LocalStorage);
  user: string;
  redirectBackTo;
  
  constructor(private http: Http) {
    if (tokenNotExpired()) {
      this.local.get('id_token').then((token) => this.authSuccess(token));
    }
  }
  
  login(credentials: {username: string, password: string}): Observable<any> {
    let loginRequest = this.http.post(
      this.loginUrl,
      JSON.stringify(credentials),
      { headers: this.contentHeader })
    .map(res => res.json())
    .share();
    
    loginRequest.subscribe(
      data => this.authSuccess(data.id_token),
      () => {}
    );
    return loginRequest
  }
  
  signup(credentials: {username: string, password: string}): Observable<any> {
    let signupRequest = this.http.post(
      this.signupUrl,
      JSON.stringify(credentials),
      { headers: this.contentHeader })
    .map(res => res.json())
    .share();
    
    signupRequest.subscribe(
        data => this.authSuccess(data.id_token),
        () => {}
    );
    return signupRequest;
  }
  
  authSuccess(token) {
    this.local.set('id_token', token);
    this.user = this.jwtHelper.decodeToken(token).username;
  }
  
  logout() {
    this.user = null;
    this.local.remove('id_token');
  }
  
  loggedIn() {
    return tokenNotExpired();
  }
  
}