import {LoginPage} from './login/login';

export function RequiresAuthentication() {
  return function (cls) {
    let originalOnPageWillEnter = cls.prototype.onPageWillEnter;
      
    cls.prototype.onPageWillEnter = function() {
      if (this.authenticationService && this.nav) {
        if (!this.authenticationService.loggedIn()) {
          this.nav.push(LoginPage, {
            authenticationService: this.authenticationService
          });
        }
        
        return;
      }
      
      if (originalOnPageWillEnter) {
        return originalOnPageWillEnter();
      }
    }
      
    return cls;
  };
}