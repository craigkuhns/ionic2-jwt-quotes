import {Page, NavParams, NavController} from 'ionic-framework/ionic';
import {AuthenticationService} from '../authentication-service';

@Page({
  templateUrl: 'build/authentication/login/login.html',
})
export class LoginPage {
  authType: string = "login";
  loginError: string;
  signupError: string;
  authenticationService: AuthenticationService;
  
  constructor(navParams: NavParams, private nav: NavController) {
    this.authenticationService = navParams.get('authenticationService');
  }
  
  signup(credentials: {username: string, password: string}) {
    this.authenticationService.signup(credentials).subscribe(
      data => this.nav.pop(),
      error => this.signupError = this.getErrorString(error)
    );
  }
  
  
  login(credentials: {username: string, password: string}) {
    this.authenticationService.login(credentials).subscribe(
      data => this.nav.pop(),
      error => this.loginError = this.getErrorString(error)
    );
  }

  getErrorString(error:any):string {
    if (error.status == 400) {
      return error._body;
    } else {
      return error.json().message;
    }
  }

}