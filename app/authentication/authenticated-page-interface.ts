import {AuthenticationService} from './authentication-service';
import {NavController} from 'ionic-framework/ionic'

export interface AuthenticatedPageInterface {
  authenticationService: AuthenticationService;
  nav: NavController;
}